package com.ericsoft.demo;

import java.util.Date;

import com.ericsoft.bmob.bson.BSON;
import com.ericsoft.bmob.bson.BSONObject;
import com.ericsoft.bmob.restapi.Bmob;

public class TestDemo {

	public static void main(String[] args) {


		//BSONObject 简单使用
//		CreateClassBSONObject();

		initBmob();//初始化
//		Search();//查询
		System.out.printf(update());//修改
//		delete();//删除
//		insert();//新增
//		callFunction();//调用云代码
//		findPayOrder();//查询支付订单
//		count();//计数

//		System.out.printf(sendSMS("13512345678"));
	}

	//使用RestAPI前必须先初始化，KEY可在Bmob应用信息里查询。
	private static void initBmob(){
		Bmob.initBmob("Application ID",
				"REST API Key");
		//用到超级权限需要注册该Key
		Bmob.initMaster("Master Key");
	}
	private static void Search(){
		//where方法很多，可参考官网RestAPI文档
		BSONObject where1 = new BSONObject(Bmob.whereLess(10));
		BSONObject where = new BSONObject();
		where.put("score", where1);
		//find方法很多，可自行尝试
		String result = Bmob.find("Your TableName", where, 0, 50, "order");
		Bmob.findBQL("BQL");
		Bmob.findBQL("BQL", "value");
		//可使用JSON 或者 BSON 转换成Object
//		BSONObject bson = new BSONObject(result);
	}

	private static String update(){
		BSONObject bson = new BSONObject();
		bson.put("username", "ddd");
		//score 修改为100
		return Bmob.update("_User", "LVif999D", bson);
	}

	private static void delete(){
		Bmob.delete("Your TableName", "Your objectId");
	}

	private static void insert(){
		BSONObject bson = new BSONObject();
		bson.put("username", "zhangsan");
		bson.put("password", "123456");
		bson.put("email", "aaa@aaa.dd");
		Bmob.insert("_User", bson);
	}

	private static String sendSMS(String phone){
		BSONObject bson = new BSONObject();
		bson.put("mobilePhoneNumber", phone);
		bson.put("template", "通知");
		return Bmob.sendSMS(bson);
	}

	private static void callFunction(){
		BSONObject bson = new BSONObject();
		bson.put("param1", "a");
		bson.put("param2", 0);
		bson.put("param3", true);

		Bmob.callFunction("Your functionName", bson);
	}

	private static void findPayOrder(){
		Bmob.findPayOrder("Your PayId");
	}

	private static void count(){
		BSONObject where = new BSONObject();
		where.put("score", 100);
		Bmob.count("Your TableName", where);
	}

	private static void CreateClassBSONObject(){
		BSONObject class1, teacher, students;
		BSONObject zhangsan, lisi, wangwu;

		class1 = new BSONObject();
		class1.put("name", "Class 1");
		class1.put("build", new Date());

		teacher = new BSONObject();
		teacher.put("name", "Miss Wang");
		teacher.put("sex", "female");
		teacher.put("age", 30);
		teacher.put("offer", true);

		students = new BSONObject();
		students.put("number", 45);
		students.put("boy", 23);
		students.put("girl", 22);

		zhangsan = new BSONObject();
		zhangsan.put("name", "ZhangSan");
		zhangsan.put("age", 12);
		zhangsan.put("sex", "male");

		lisi = new BSONObject();
		lisi.put("name", "LiSi");
		lisi.put("age", 12);
		lisi.put("sex", "female");

		wangwu = new BSONObject();
		wangwu.put("name", "WangWu");
		wangwu.put("age", 13);
		wangwu.put("sex", "male");

		students.put("student", new BSONObject[]{zhangsan, lisi, wangwu});
		class1.put("teacher", teacher);
		class1.put("students", students);

		BSONObject bson_class = new BSONObject(class1.toString());
		BSON.Log(bson_class+"\n");
		BSON.Log(bson_class.getDate("build"));
		BSON.Log(bson_class.getBSONObject("teacher").getBoolean("offer"));
		BSON.Log(bson_class.getBSONObject("teacher").getInt("age"));
		BSON.Log(bson_class.getBSONObject("students").getBSONArray("student")[0].getString("name"));

	}

}
